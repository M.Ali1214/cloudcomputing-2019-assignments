from builtins import Exception, len

import pymysql

NOTSTARTED = 'Not Started'
INPROGRESS = 'In Progress'
COMPLETED = 'Completed'
mydb = pymysql.connect(host='127.0.0.1', port=3306, user='root',
    password='watches', db='watches', charset="utf8")
mycursor = mydb.cursor()
mycursor.execute("SELECT * FROM watches")
myresult = mycursor.fetchall()

# for x in myresult:
#   print(x)

def add_to_list(_sku, _type_watch, _status, _gender, _year, _dial_material, _dial_color, _case_material, _case_form,
                _bracelet_material, _movement):
    try:
        c = mydb.cursor()
        c.execute(
            "INSERT INTO watches(sku,type,status,gender,year, dial_material, dial_color, case_material, case_form, bracelet_material, movement) VALUES(%s, %s, %s, %s,%s,%s,%s,%s,%s,%s,%s)",
            (_sku, _type_watch, _status, _gender, _year, _dial_material, _dial_color, _case_material, _case_form,
             _bracelet_material, _movement))
        mydb.commit()
        return {"val": _sku, "status": NOTSTARTED}
    except Exception as e:
        print('Error: ', e)
        return None



def get_watch(value):
    try:
        mycursor = mydb.cursor()
        mycursor.execute("SELECT * FROM watches WHERE sku=%s", value)
        myresult = mycursor.fetchall()
        return { "count": len(myresult), "items": myresult }
    except Exception as e:
        print('Error: ', e)
        return None


def get_by_pref(value):
    try:
        mycursor = mydb.cursor()
        mycursor.execute("SELECT * FROM watches WHERE sku LIKE %s", (value + "%"))
        myresult = mycursor.fetchall()
        return { "count": len(myresult), "items": myresult }
    except Exception as e:
        print('Error: ', e)
        return None


def generate_sql(dictionary):
    if len(dictionary) == 1:
        if len(dictionary.get('sku')) != 0:
            sql = 'SELECT * FROM watches WHERE ' + list(dictionary.items())[0][0] + ' LIKE %s'
            return sql
        else:
            sql = 'SELECT * FROM watches WHERE ' + list(dictionary.items())[0][0] + '=%s'
            return sql
    else:
        sql = 'SELECT * FROM watches WHERE'
        n = 0
        for i in dictionary.items():
            if n == 0:
                if len(dictionary.get('sku')) != 0:
                    sql = sql + " " + i[0] + ' LIKE %s'
                else:
                    sql = sql + " " + i[0] + '=%s'
            else:
                if n == (len(dictionary) - 1):
                    sql = sql + " AND " + i[0] + '=%s'
                else:
                    sql = sql + " AND " + i[0] + '=%s'
            n = n + 1
        return (sql)


def get_by_criteria(_sku, _type_watch, _status, _gender, _year, _dial_material, _dial_color, _case_material, _case_form,
                    _bracelet_material, _movement, dict_obj):
    sql = generate_sql(dict_obj)
    values = []
    list_vals = []
    for i in dict_obj.items():
        values.append(i[0])
    for i in values:
        if i == "sku":
            list_vals.append("%" + dict_obj.get(i) + "%")
        else:
            list_vals.append(dict_obj.get(i))
    try:
        mycursor = mydb.cursor(prepared=True)
        mycursor.execute(sql, tuple(list_vals))
        myresult = mycursor.fetchall()
        return myresult
    except Exception as e:
        print('Error: ', e)
        return None

def delete_watch(sku):
        try:
            conn = mydb
            c = conn.cursor()
            c.execute(
                "DELETE FROM watches WHERE sku = %s",
                (sku))
            conn.commit()
            return {'sku': sku}
        except Exception as e:
            print('Error: ', e)
            return None

def update_status(_sku, _type_watch, _status, _gender, _year, _dial_material, _dial_color, _case_material, _case_form,
                  _bracelet_material, _movement):
    try:
        conn = mydb
        c = conn.cursor()
        c.execute(
            "UPDATE watches SET type=%s,status=%s,gender=%s,year=%s,dial_material=%s,dial_color=%s,case_material=%s,case_form=%s,bracelet_material=%s,movement=%s WHERE sku=%s",
            (_type_watch, _status, _gender, _year, _dial_material, _dial_color, _case_material, _case_form,
             _bracelet_material, _movement, _sku))
        conn.commit()
        return {'sku': _sku}
    except Exception as e:
        print('Error: ', e)
        return 'Failure'

def get_watch_by_sku(value):
    try:
        mycursor = mydb.cursor()
        mycursor.execute("SELECT sku FROM watches WHERE sku=%s", value)
        myresult = mycursor.fetchall()
        return { "count": len(myresult), "items": myresult }
    except Exception as e:
        print('Error: ', e)
        return None

